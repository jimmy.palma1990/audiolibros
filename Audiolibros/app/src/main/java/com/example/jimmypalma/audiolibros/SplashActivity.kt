package com.example.jimmypalma.audiolibros

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar

/**
 * Created by jimmypalma on 2/21/19.
 */
class SplashActivity: AppCompatActivity() {
    lateinit var progressBar: ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)

        progressBar = findViewById(R.id.progress_bar)
        progressBar.visibility = View.VISIBLE
        val handler = Handler()
        handler.postDelayed(Runnable {
            val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)

        }, 4000)   //4 seconds

    }
}