package com.example.jimmypalma.audiolibros.fragments

import android.app.Fragment
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import com.example.jimmypalma.audioBooks.entities.Book
import com.example.jimmypalma.audiolibros.Aplicacion
import com.example.jimmypalma.audiolibros.R
import com.example.jimmypalma.audiolibros.UI.ZoomSeekBar
import kotlinx.android.synthetic.main.fragment_detalle.view.*


/**
 * Created by jimmypalma on 2/2/19.
 */
class DetalleFragment: Fragment(), View.OnTouchListener, MediaPlayer.OnPreparedListener,
        MediaController.MediaPlayerControl, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener {

    lateinit var barraObj : ZoomSeekBar
    override fun onBufferingUpdate(p0: MediaPlayer?, p1: Int) {


        if (p0 != null) {
            Log.d("Audiolibros", "------------------duracion " + p0.duration)
            Log.d("Audiolibros", "------------------porcentaje " + p0.currentPosition)
            Log.d("Audiolibros", "------------------total " + p0.duration * p1/ 100)
            barraObj.setNuevoValor(p1)
        }
    }

    override fun onCompletion(p0: MediaPlayer?) {
        Log.d("Audiolibros", "------------------Entro onCompletion")
    }

    companion object {
        const val ARG_ID_LIBRO = "id_libro"
    }

    val mediaPlayer = MediaPlayer()
    var mediaController: MediaController? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var vistaDetalle: View = inflater?.inflate(R.layout.fragment_detalle, container, false)

        val args = arguments
        if (args != null) {
            val position = args.getInt(ARG_ID_LIBRO)
            ponInfoLibro(position, vistaDetalle)
        } else {
            ponInfoLibro(0, vistaDetalle)
        }

        barraObj = vistaDetalle.findViewById(R.id.barrax)


        return vistaDetalle
    }

    fun ponInfoLibro (id: Int, vista: View?) {

        if(vista != null) {
            val libro: Book = (activity.application as Aplicacion)
                    .listaLibros[id]

            vista.titulo.text = libro.tituloBook
            vista.autor.text = libro.autorBook
            vista.portada_detalle.setImageResource(libro.recursoImagenBook)

            vista.setOnTouchListener(this)

            if (mediaPlayer != null) {

                mediaPlayer.reset()
                mediaPlayer.setOnPreparedListener(this)
                mediaPlayer.setOnBufferingUpdateListener(this)
            }


            try {
                mediaPlayer.setDataSource(libro.urlAudioBook) // setup song from https://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepareAsync()
                // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun ponInfoLibro(id: Int) {
        ponInfoLibro(id, view)
    }

    override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
//        mediaController?.show()
        Log.d("Audiolibros", "------------------ entro onTouch")
            mediaPlayer.start()
        return false
    }

    override fun onPrepared(p0: MediaPlayer?) {
        Log.d("Audiolibros", "------------------ entro onPrepared")
//        mediaPlayer.start();
//        mediaController?.setMediaPlayer(this)
//        mediaController?.setAnchorView(getView().findViewById(R.id.fragment_detalle))
//        mediaController?.show()

    }

    override fun onStop() {
        mediaController?.hide()
        try {
            mediaPlayer.stop()
            mediaPlayer.release()
        } catch (e: Exception) {
            Log.d("Audiolibros", "Error en mediaPlayer.stop()")
        }

        super.onStop()
    }

    override fun canPause(): Boolean {
        Log.d("Audiolibros", "------------------ entro canPause")
        return true
    }

    override fun canSeekForward(): Boolean {
        Log.d("Audiolibros", "------------------ entro canPause")
        return true
    }

    override fun getBufferPercentage(): Int {
        Log.d("JIMMY", mediaPlayer.currentPosition.toString())
        return 0
    }

    override fun getCurrentPosition(): Int {
        Log.d("Audiolibros", "------------------ entro getCurrentPosition")
        try {
            return mediaPlayer.currentPosition
        } catch (e: Exception) {
            return 0
        }
    }

    override fun getDuration(): Int {
        return mediaPlayer.getDuration()
    }

    override fun isPlaying(): Boolean {
        return mediaPlayer.isPlaying();
    }

    override fun canSeekBackward(): Boolean {
        return true
    }

    override fun pause() {
        mediaPlayer.pause()
    }

    override fun seekTo(p0: Int) {
        mediaPlayer.seekTo(p0)
    }

    override fun start() {
        mediaPlayer.start()
    }

    override fun getAudioSessionId(): Int {
        return 0
    }
}