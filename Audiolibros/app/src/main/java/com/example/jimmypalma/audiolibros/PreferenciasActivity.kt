package com.example.jimmypalma.audiolibros

import android.app.Activity
import android.os.Bundle
import com.example.jimmypalma.audiolibros.fragments.PreferenciasFragment

/**
 * Created by jimmypalma on 2/20/19.
 */
class PreferenciasActivity: Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentManager.beginTransaction().replace(android.R.id.content, PreferenciasFragment()).commit()
    }
}