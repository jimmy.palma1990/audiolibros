package com.example.jimmypalma.audiolibros.interfaces.impl

import com.example.jimmypalma.audiolibros.MainActivity
import com.example.jimmypalma.audiolibros.interfaces.ClickAction

/**
 * Created by jimmypalma on 3/27/19.
 */
class OpenDetailClickAction(mainActivity: MainActivity): ClickAction {


    val mainActivity = mainActivity
    override fun execute(position: Int) {
        mainActivity.mostrarDetalle(position)
    }
}