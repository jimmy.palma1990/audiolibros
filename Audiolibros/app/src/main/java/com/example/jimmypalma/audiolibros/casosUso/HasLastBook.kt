package com.example.jimmypalma.audiolibros.casosUso

import com.example.jimmypalma.audiolibros.repository.BooksRespository

/**
 * Created by jimmypalma on 4/3/19.
 */
//class HasLastBook(librosStorage: LibroStorage) {
class HasLastBook(booksRespository: BooksRespository) {
//    val librosStorage: LibroStorage = librosStorage
    val booksRespository: BooksRespository = booksRespository

    fun execute(): Boolean {
//        return librosStorage.hasLastBook()
        return booksRespository.hasLastBook()
    }
}