package com.example.jimmypalma.audiolibros.interfaces

/**
 * Created by jimmypalma on 3/27/19.
 */
interface ClickAction {
    fun execute( position: Int)
}