package com.example.jimmypalma.audiolibros.data

import android.content.Context
import android.content.SharedPreferences
import com.example.jimmypalma.audiolibros.interfaces.LibroStorageInterface

/**
 * Created by jimmypalma on 3/25/19.
 */
class LibroStorage (context: Context): LibroStorageInterface {

    lateinit var instance: LibroStorage
    companion object {
        const val PREF_AUDIOLIBROS ="com.example.audiolibros_internal"
        const val KEY_ULTIMO_LIBRO = "ultimo"
    }
    var context: Context = context

    fun getInstance(context: Context): LibroStorage {
        if(instance == null) {
            instance = LibroStorage(context)
        }
        return instance
    }

    fun getPreference(): SharedPreferences {
        return context.getSharedPreferences(PREF_AUDIOLIBROS, Context.MODE_PRIVATE)
    }

    override fun hasLastBook(): Boolean {
        return getPreference().contains(KEY_ULTIMO_LIBRO)
    }

    override fun getLastBook(): Int {
        return getPreference().getInt(KEY_ULTIMO_LIBRO, -1)
    }

    override fun saveLastBook(id: Int) {
        val editor = getPreference().edit()
        editor.putInt("ultimo", id)
        editor.commit()
    }
}