package com.example.jimmypalma.audiolibros.fragments

import android.app.Activity
import android.app.Fragment
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import com.example.jimmypalma.audioBooks.entities.Book
import com.example.jimmypalma.audiolibros.Aplicacion
import com.example.jimmypalma.audiolibros.MainActivity
import com.example.jimmypalma.audiolibros.R
import com.example.jimmypalma.audiolibros.data.adaptars.BooksAdapterFilter
import com.example.jimmypalma.audiolibros.data.observables.SearchObservable




/**
 * Created by jimmypalma on 2/2/19.
 */
class SelectorFragment: Fragment() {
    lateinit var actividad: Activity
    lateinit var adaptador: BooksAdapterFilter
    lateinit var listaLibros: MutableList<Book>

    override fun onAttach(actividad: Activity) {
        super.onAttach(actividad)
        this.actividad = actividad
        val app = actividad.application as Aplicacion
        adaptador = app.adaptador
        listaLibros = app.listaLibros
    }

    override fun  onCreateView(inflador: LayoutInflater, contenedor: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        var vista: View = inflador?.inflate(R.layout.fragment_layout, contenedor, false)

        var recycler_view = vista.findViewById<View>(R.id.recycler_view) as RecyclerView

        setHasOptionsMenu(true)

        recycler_view.layoutManager = GridLayoutManager(actividad, 2)
        recycler_view.adapter = adaptador
        recycler_view.setBackgroundColor(Color.CYAN)
        return vista;
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_selector, menu)
        val searchItem = menu.findItem(R.id.menu_buscar)
        val searchView = searchItem.getActionView() as SearchView
//        searchView.setOnQueryTextListener(
//                object : SearchView.OnQueryTextListener {
//                    override fun onQueryTextChange(query: String): Boolean {
//                        adaptador.setTextSearch(query)
//                        adaptador.notifyDataSetChanged()
//                        return false
//                    }
//
//                    override fun onQueryTextSubmit(query: String): Boolean {
//                        return false
//                    }
//                })

        var searchObservable = SearchObservable()
        searchObservable.addObserver(adaptador)
        searchView.setOnQueryTextListener(searchObservable)

        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                adaptador.setTextSearch("")
                adaptador.notifyDataSetChanged()
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                // TODO: do something...
                return true
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        if (id == R.id.menu_ultimo) {
            (actividad as MainActivity).irUltimoVisitado()
            return true
        } else if (id == R.id.menu_buscar) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}