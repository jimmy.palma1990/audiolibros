package com.example.jimmypalma.audiolibros.casosUso

import com.example.jimmypalma.audiolibros.repository.BooksRespository

/**
 * Created by jimmypalma on 4/3/19.
 */
//class SaveLastBook(librosStorage: LibroStorage) {
class SaveLastBook(booksRespository: BooksRespository) {
//    val librosStorage: LibroStorage = librosStorage
    val booksRespository: BooksRespository = booksRespository

    fun execute(id: Int) {
//        librosStorage.saveLastBook(id)
        booksRespository.saveLastBook(id)
    }
}