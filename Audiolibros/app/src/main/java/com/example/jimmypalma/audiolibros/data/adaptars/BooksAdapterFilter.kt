package com.example.jimmypalma.audiolibros.data.adaptars

import android.content.Context
import com.example.jimmypalma.audioBooks.entities.Book
import java.util.*


/**
 * Created by jimmypalma on 2/5/19.
 */
class BooksAdapterFilter(booksList : List<Book>, context: Context): BooksAdapter(booksList, context), Observer {
    override fun update(p0: Observable?, p1: Any?) {
        setTextSearch(p1 as String)
        notifyDataSetChanged()
    }

    var listaSinFiltro: List<Book> = booksList // Lista con todos los libros
    var indiceFiltro: List<Int>? = null
    var busqueda: String? = ""
    var genero:String = ""
    var novedad: Boolean = false
    var leido: Boolean = false

    init {
        recalculaFiltro()
    }

    fun setTextSearch(busqueda: String) {
        this.busqueda = busqueda.toLowerCase()
        recalculaFiltro()
    }
    fun setGender(genero: String) {
        this.genero = genero.toLowerCase()
        recalculaFiltro()
    }

    fun setNovelty(novedad: Boolean) {
        this.novedad = novedad
        recalculaFiltro()
    }

    fun setRead(leido: Boolean) {
        this.leido = leido
        recalculaFiltro()
    }

    fun recalculaFiltro() {
        listaLibros = ArrayList<Book>()
        indiceFiltro = ArrayList()
        for (i in 0 until listaSinFiltro!!.count()) {
            val libro = listaSinFiltro!!.get(i)
            if ((libro.tituloBook!!.toLowerCase().contains(busqueda.toString()) || libro.autorBook.toLowerCase().contains(busqueda.toString()))
                    && libro.generoBook!!.toLowerCase().startsWith(genero)
                    && (!novedad || novedad && libro.novedadBook!!)
                    && (!leido || leido && libro.leidoBook!!)) {
                (listaLibros as ArrayList<Book>).add(libro)
                (indiceFiltro as ArrayList<Int>).add(i)
            }
        }
    }
    fun getItem(posicion: Int): Book {
            return listaSinFiltro!!.get(indiceFiltro!!.get(posicion))
    }
    override fun getItemId(posicion: Int): Long {
        return indiceFiltro!!.get(posicion).toLong()
    }
}