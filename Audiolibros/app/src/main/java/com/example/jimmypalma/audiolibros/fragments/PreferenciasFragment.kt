package com.example.jimmypalma.audiolibros.fragments

import android.os.Bundle
import android.preference.PreferenceFragment
import com.example.jimmypalma.audiolibros.R

/**
 * Created by jimmypalma on 2/20/19.
 */
class PreferenciasFragment: PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferences)
    }
}