package com.example.jimmypalma.audiolibros.interfaces

/**
 * Created by jimmypalma on 3/28/19.
 */
interface LibroStorageInterface {
    fun hasLastBook(): Boolean
    fun getLastBook(): Int
    fun saveLastBook(id: Int)
}