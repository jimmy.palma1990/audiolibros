package com.example.jimmypalma.audioBooks.entities


/**
 * Created by jimmypalma on 2/1/19.
 */
class Book(private var titulo: String, private var autor: String, private var recursoImagen: Int,
            private var urlAudio: String, private var genero: String, private var novedad: Boolean?,
            private var leido: Boolean?) {

    val tituloBook: String? = titulo
    val autorBook: String = autor
    val recursoImagenBook: Int = recursoImagen
    val urlAudioBook: String = urlAudio
    val generoBook: String? = genero
    val novedadBook: Boolean? = novedad
    val leidoBook: Boolean? = leido

    companion object {
        const val G_TODOS = "Todos los géneros"
        const val G_EPICO = "Poema épico"
        const val G_S_XIX = "Literatura siglo XIX"
        const val G_SUSPENSE = "Suspense"
        val G_ARRAY = Array<String>(4) { Book.G_TODOS; Book.G_EPICO; Book.G_S_XIX; Book.G_SUSPENSE }
    }
}
