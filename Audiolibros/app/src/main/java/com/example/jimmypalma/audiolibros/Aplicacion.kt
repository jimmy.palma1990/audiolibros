package com.example.jimmypalma.audiolibros

import android.app.Application
import com.example.jimmypalma.audioBooks.entities.Book
import com.example.jimmypalma.audiolibros.data.BooksData
import com.example.jimmypalma.audiolibros.data.adaptars.BooksAdapterFilter

/**
 * Created by jimmypalma on 2/1/19.
 */
class Aplicacion: Application(){
    lateinit var listaLibros: MutableList<Book>
    lateinit var adaptador: BooksAdapterFilter

    override fun onCreate() {
        super.onCreate()
        listaLibros = BooksData.ejemploBooks()
        adaptador = BooksAdapterFilter(listaLibros, this)
    }
}