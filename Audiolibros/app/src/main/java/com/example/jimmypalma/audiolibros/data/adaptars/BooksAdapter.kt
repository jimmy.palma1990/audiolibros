package com.example.jimmypalma.audiolibros.data.adaptars

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jimmypalma.audioBooks.entities.Book
import com.example.jimmypalma.audiolibros.R
import com.example.jimmypalma.audiolibros.interfaces.ClickAction
import kotlinx.android.synthetic.main.content_main.view.*

/**
 * Created by jimmypalma on 1/29/19.
 */
open class BooksAdapter(open val booksList : List<Book>, open val context: Context): RecyclerView.Adapter<ViewHolder>() {

    //var onClickListener: View.OnClickListener? = null
    var onLongClickListener: View.OnLongClickListener? = null
    lateinit var clickActions: ClickAction
    fun setClickAction(clickAction: ClickAction){
        clickActions = clickAction
    }

    var listaLibros : List<Book>? = null

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return listaLibros!!.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        listaLibros = booksList
        var v: View = LayoutInflater.from(context).inflate(R.layout.content_main, parent, false)
//        v.setOnClickListener(onClickListener);
        v.setOnLongClickListener(onLongClickListener);
        return ViewHolder(v)
    }

    // Binds each animal in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        var libro: Book = listaLibros!!.get(position)
        holder?.bookName?.text = libro.tituloBook
        holder?.portada?.setImageResource(libro.recursoImagenBook)
        holder?.itemView?.setOnClickListener{
            clickActions.execute(position)
        }
    }

//    fun setOnItemClickListener(onClickListener: View.OnClickListener) {
//        this.onClickListener = onClickListener
//    }

    fun setOnItemLongClickListener(onLongClickListener: View.OnLongClickListener) {
        this.onLongClickListener = onLongClickListener
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

    // Holds the TextView that will add each animal to
    val bookName = view.bookName
    val portada = view.portada
}