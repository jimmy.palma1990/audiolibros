package com.example.jimmypalma.audiolibros

import com.example.jimmypalma.audiolibros.casosUso.GetLastBook
import com.example.jimmypalma.audiolibros.casosUso.HasLastBook
import com.example.jimmypalma.audiolibros.casosUso.SaveLastBook

/**
 * Created by jimmypalma on 4/1/19.
 */
//class MainPresenter(libroStorage: LibroStorage, view: MainPresenter.View) {
class MainPresenter(saveLastBook: SaveLastBook, hasLastBook: HasLastBook, getLastBook: GetLastBook, view: MainPresenter.View) {
    val saveLastBook: SaveLastBook = saveLastBook
//    val libroStorage: LibroStorage = libroStorage
    val hasLastBook: HasLastBook = hasLastBook
    val getLastBook: GetLastBook = getLastBook
    val view: View = view

    fun clickFavoriteButton() {
//        if (libroStorage.hasLastBook()) {
        if (hasLastBook.execute()) {
//            view.mostrarDetalle(libroStorage.getLastBook())
            view.mostrarDetalle(getLastBook.execute())
        } else {
            view.mostrarNoUltimaVisita()
        }

        fun openDetalle(id: Int) {
            //libroStorage.saveLastBook(id)
            saveLastBook.execute(id)
            view.mostrarDetalle(id)
        }
    }

    interface View {
        fun mostrarDetalle(lastBook: Int)
        fun mostrarNoUltimaVisita()
    }
}