package com.example.jimmypalma.audiolibros

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.example.jimmypalma.audioBooks.entities.Book
import com.example.jimmypalma.audiolibros.casosUso.GetLastBook
import com.example.jimmypalma.audiolibros.casosUso.HasLastBook
import com.example.jimmypalma.audiolibros.casosUso.SaveLastBook
import com.example.jimmypalma.audiolibros.data.LibroStorage
import com.example.jimmypalma.audiolibros.fragments.DetalleFragment
import com.example.jimmypalma.audiolibros.fragments.PreferenciasFragment
import com.example.jimmypalma.audiolibros.fragments.SelectorFragment
import com.example.jimmypalma.audiolibros.interfaces.impl.OpenDetailClickAction
import com.example.jimmypalma.audiolibros.repository.BooksRespository
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_layout.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, MainPresenter.View {
    override fun mostrarNoUltimaVisita() {
        Toast.makeText(this, "Sin última vista",Toast.LENGTH_LONG).show()
    }

    lateinit var app: Aplicacion
    //var bookStorage = LibroStorage(this)
    val controller = MainController(LibroStorage(this))
    val presenter = MainPresenter(SaveLastBook(BooksRespository(LibroStorage(this))), HasLastBook(BooksRespository(LibroStorage(this))), GetLastBook(BooksRespository(LibroStorage(this))), this)

    override fun onBackPressed() {
        val drawer = findViewById<View>(
                R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        app = application as Aplicacion

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.app_name,
                R.string.drawer_close)

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        fab.setOnClickListener { view ->
            //irUltimoVisitado()
            presenter.clickFavoriteButton()
        }

        tabs.addTab(tabs.newTab().setText("Todos"))
        tabs.addTab(tabs.newTab().setText("Nuevos"))
        tabs.addTab(tabs.newTab().setText("Leidos"))
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE)
        tabs.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
                        app.adaptador.setNovelty(false)
                        app.adaptador.setRead(false)
                    }
                    1 -> {
                        app.adaptador.setNovelty(true)
                        app.adaptador.setRead(false)
                    }
                    2 -> {
                        app.adaptador.setNovelty(false)
                        app.adaptador.setRead(true)
                    }
                }
                app.adaptador.notifyDataSetChanged()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }

        })

//        app.adaptador.setOnItemClickListener(View.OnClickListener { v ->
//            mostrarDetalle(recycler_view.getChildAdapterPosition(v))
//        })

        app.adaptador.setClickAction(OpenDetailClickAction(this))
        longClick()

        fragmentManager.beginTransaction().add(this.getIdContenedor(), SelectorFragment()).commit()

    }

    private fun longClick() {
        app.adaptador.setOnItemLongClickListener(View.OnLongClickListener { v ->
            val id = recycler_view.getChildAdapterPosition(v)
            val menu = android.app.AlertDialog.Builder(this)
            val opciones = arrayOf<CharSequence>("Compartir", "Borrar ", "Insertar")
            menu.setItems(opciones, DialogInterface.OnClickListener { dialog, opcion ->
                when (opcion) {
                    0 //Compartir
                    -> {
                        val libro = app.listaLibros.get(id)
                        val i = Intent(Intent.ACTION_SEND)
                        i.type = "text/plain"
                        i.putExtra(Intent.EXTRA_SUBJECT, libro.tituloBook)
                        i.putExtra(Intent.EXTRA_TEXT, libro.urlAudioBook)
                        startActivity(Intent.createChooser(i, "Compartir"))
                    }
                    1 //borrar
                    -> {
                        val snack = Snackbar.make(v,"Desea borrar el libro?", Snackbar.LENGTH_LONG)
                        snack.setAction("SI", View.OnClickListener {
                            app.listaLibros.removeAt(id)
                            app.adaptador.notifyDataSetChanged()
                        })
                        snack.show()
                    }
                    2 -> { //insertar
                        app.listaLibros.add(app.listaLibros.get(id))
                        app.adaptador.notifyDataSetChanged()
                        val snack = Snackbar.make(v, "Libro insertado", Snackbar.LENGTH_INDEFINITE)
                        snack.setAction("ok", View.OnClickListener {

                        })
                        snack.show()
                    }
                }
            })
            menu.create().show()
            true
        })
    }
    private fun getIdContenedor() : Int {
        var idContenedor: Int = 0
        if(findViewById<LinearLayout>(R.id.contenedor_pequenio) != null && (supportFragmentManager.findFragmentById(
                        R.id.contenedor_pequenio) == null)) {
            idContenedor = R.id.contenedor_pequenio
        } else {
            idContenedor = R.id.contenedor_izquierdo
        }
        return idContenedor
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        if (id == R.id.menu_preferencias) {
            fragmentManager.beginTransaction().replace(this.getIdContenedor(), PreferenciasFragment()).addToBackStack(null).commit()
            return true
        } else if (id == R.id.menu_ultimo) {
            irUltimoVisitado()
            return true
        } else if (id == R.id.menu_buscar) {
            return true
        } else if (id == R.id.menu_acerca) {
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Mensaje de Acerca De")
            builder.setPositiveButton(android.R.string.ok, null)
            builder.create().show()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun irUltimoVisitado() {
//        if (bookStorage.hasLastBook()) {
//            mostrarDetalle(bookStorage.getLastBook())
//        } else {
//            Toast.makeText(this,"Sin última vista",Toast.LENGTH_LONG).show()
//        }
        presenter.clickFavoriteButton()
    }

    fun addFragment(fragment: Fragment, addToBackStack: Boolean, tag: String) {
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()

        if (addToBackStack) {
            ft.addToBackStack(tag)
        }
        ft.replace(R.id.contenedor_pequenio, fragment, tag)
        ft.commitAllowingStateLoss()
    }

    override fun mostrarDetalle(id: Int) {
        mostrarFragmentDetalle(id)
        controller.saveLastBook(id)
    }

    fun mostrarFragmentDetalle(id: Int) {
        val manager = fragmentManager
        var detalleFragment  = manager.findFragmentById(R.id.detalle_fragment)
        if (detalleFragment != null) {
            (detalleFragment as DetalleFragment).ponInfoLibro(id)
        } else {
            val nuevoFragment = DetalleFragment()
            val args = Bundle()
            args.putInt(DetalleFragment.ARG_ID_LIBRO, id)
            nuevoFragment.arguments = args
            val transaccion = manager
                    .beginTransaction()
            transaccion.replace(R.id.contenedor_pequenio, nuevoFragment)
            transaccion.addToBackStack(null)
            transaccion.commit()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var id: Int = item.getItemId();
        if (id == R.id.nav_todos) {
            app.adaptador.setGender("")
            app.adaptador.notifyDataSetChanged()
        } else if (id == R.id.nav_epico) {
            app.adaptador.setGender(Book.G_EPICO)
            app.adaptador.notifyDataSetChanged()
        } else if (id == R.id.nav_XIX) {
            app.adaptador.setGender(Book.G_S_XIX);
            app.adaptador.notifyDataSetChanged()
        } else if (id == R.id.nav_suspense) {
            app.adaptador.setGender(Book.G_SUSPENSE);
            app.adaptador.notifyDataSetChanged()
        }
        drawer_layout.closeDrawer(GravityCompat.START);
        return true;
    }
}
