package com.example.jimmypalma.audiolibros.UI

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.example.jimmypalma.audiolibros.R


/**
 * Created by jimmypalma on 2/8/19.
 */
class ZoomSeekBar: View {

    companion object {
        const val SIN_PULSACION = "SIN_PULSACION"
        const val PALANCA_PULSADA = "PALANCA_PULSADA"
        const  val ESCALA_PULSADA = "ESCALA_PULSADA"
        const val ESCALA_PULSADA_DOBLE = "ESCALA_PULSADA_DOBLE"
    }

    var estado = ZoomSeekBar.SIN_PULSACION
    var antVal_0: Int = 0
    var antVal_1: Int = 0
    // Valor a controlar
    var valor: Int = 0; // valor seleccionado

    fun setNuevoValor(nuevoValor: Int) {
        if (nuevoValor in valMin..valMax) {
            valor = nuevoValor
            escalaMin = Math.min(escalaMin, valor)
            escalaMax = Math.max(escalaMax, valor)
            invalidate()
        }
    }
    var valMin: Int = 0; // valor mínimo
    var valMax: Int = 200; // valor máximo
    var escalaMin: Int = 0; // valor mínimo visualizado
    var escalaMax: Int = 180; // valor máximo visualizado
    var escalaIni: Int = 0; // origen de la escala
    var escalaRaya: Int = 2; // cada cuantas unidades una rayas
    var escalaRayaLarga: Int = 5; // cada cuantas rayas una larga
    // Dimensiones en pixels
    var altoNumeros: Int = 0
    var altoRegla: Int = 0
    var altoBar: Int = 0
    var altoPalanca: Int = 0
    var anchoPalanca: Int = 0
    var altoGuia: Int = 0
    // Valores que indican donde dibujar
    var xIni: Int = 0
    var yIni: Int = 0
    var ancho: Int = 0
    // Objetos Rect con diferentes regiones
    var escalaRect = Rect()
    var barRect = Rect()
    var guiaRect = Rect()
    var palancaRect = Rect()
    // Objetos Paint globales para no tener que crearlos cada vez
    var textoPaint = Paint()
    var reglaPaint = Paint()
    var guiaPaint = Paint()
    var palancaPaint = Paint()

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val dp = resources.displayMetrics.density
        val a: TypedArray = context.theme.obtainStyledAttributes(attrs,
                R.styleable.ZoomSeekBar, 0, 0)
        try {
            altoNumeros = a.getDimensionPixelSize(R.styleable.ZoomSeekBar_altoNumeros, ((30 * dp).toInt()))
            altoRegla = a.getDimensionPixelSize(R.styleable.ZoomSeekBar_altoRegla, ((20 * dp).toInt()))
            altoBar = a.getDimensionPixelSize(R.styleable.ZoomSeekBar_altoBar, ((70 * dp).toInt()))
            altoPalanca = a.getDimensionPixelSize(R.styleable.ZoomSeekBar_altoPalanca, ((40 * dp).toInt()))
            altoGuia = a.getDimensionPixelSize(R.styleable.ZoomSeekBar_altoGuia, ((10 * dp).toInt()))
            anchoPalanca = a.getDimensionPixelSize(R.styleable.ZoomSeekBar_anchoPalanca, ((20 * dp).toInt()))
            textoPaint.textSize = a.getDimension(R.styleable.ZoomSeekBar_altoTexto, 16 * dp)
            textoPaint.setColor(a.getColor(R.styleable.ZoomSeekBar_colorTexto, Color.BLACK))
            reglaPaint.setColor(a.getColor(R.styleable.ZoomSeekBar_colorRegla, Color.BLACK))
            guiaPaint.setColor(a.getColor(R.styleable.ZoomSeekBar_colorGuia, Color.BLUE))
            palancaPaint.setColor(a.getColor(R.styleable.ZoomSeekBar_colorPalanca, 0xFF00007F.toInt()))
        } finally {
            textoPaint.setAntiAlias(true)
            textoPaint.setTextAlign(Paint.Align.CENTER)
            textoPaint.textSize = 16 * dp
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        xIni = this.paddingLeft
        yIni = this.paddingTop
        ancho = width - paddingRight - paddingLeft
        barRect.set(xIni, yIni, xIni+ancho, yIni+altoBar)
        escalaRect.set(xIni, yIni + altoBar, xIni + ancho, yIni + altoBar + altoNumeros + altoRegla)
        var y: Int = yIni + (altoBar - altoGuia) / 2
        guiaRect.set(xIni, y, xIni + ancho, y + altoGuia)
    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawRect(guiaRect, guiaPaint)
        var y: Int = yIni + (altoBar - altoPalanca) / 2
        var x: Int = xIni + ancho * (valor - escalaMin) / (escalaMax - escalaMin) - anchoPalanca / 2
        palancaRect.set(x, y, x + anchoPalanca, y + altoPalanca)
        canvas.drawRect(palancaRect, palancaPaint)
        palancaRect.set(x - anchoPalanca / 2, y, x + 3 * anchoPalanca / 2, y + altoPalanca)
        // Dibujamos Escala
        var v = escalaIni
        while (v < escalaMax) {
            if ( v > escalaMin) {
                x = xIni + ancho * (v - escalaMin) / (escalaMax - escalaMin)
                if (((v - escalaIni) / escalaRaya) % escalaRayaLarga == 0) {
                    y = yIni + altoBar + altoRegla
                    canvas.drawText(Integer.toString(v), x.toFloat(), (y + altoNumeros).toFloat(), textoPaint)
                } else {
                    y = yIni + altoBar + altoRegla * 1 / 3
                }
                canvas.drawLine(x.toFloat(), (yIni + altoBar).toFloat(), x.toFloat(), y.toFloat(), reglaPaint)
//                canvas.drawLine(x.toFloat()-270, (yIni + altoBar).toFloat(), x.toFloat()-270, y.toFloat()+45, reglaPaint)
            }
            v += escalaRaya
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        var x_0: Int
        var y_0: Int
        var x_1: Int
        var y_1: Int

        if (event != null) {
            x_0 = event.getX(0).toInt()
            y_0 = event.getY(0).toInt()

            var val_0: Int = escalaMin + (x_0-xIni) * (escalaMax-escalaMin) / ancho
            if (event.pointerCount > 1) {
                x_1 = event.getX(1).toInt()
                y_1 = event.getY(0).toInt()
            } else {
                x_1 = x_0; y_1 = y_0
            }
            var val_1: Int = escalaMin + (x_1 - xIni) * (escalaMax - escalaMin) / ancho

            when (event.actionMasked) {
                MotionEvent.ACTION_DOWN -> {
                    if (palancaRect.contains(x_0, y_0)) {
                        estado = ZoomSeekBar.PALANCA_PULSADA
                    } else if (barRect.contains(x_0, y_0)) {
                        if (val_0 > valor) valor++ else valor--
                        invalidate(barRect)
                    } else if (escalaRect.contains(x_0, y_0)) {
                        estado = ZoomSeekBar.ESCALA_PULSADA
                        antVal_0 = val_0
                    }
                }
                MotionEvent.ACTION_POINTER_DOWN -> {
                    if (estado == ZoomSeekBar.ESCALA_PULSADA) {
                        if (escalaRect.contains(x_1, y_1)) {
                            antVal_1 = val_1
                            estado = ZoomSeekBar.ESCALA_PULSADA_DOBLE
                        }
                    }
                }
                MotionEvent.ACTION_UP -> {
                    estado = ZoomSeekBar.SIN_PULSACION
                }
                MotionEvent.ACTION_POINTER_UP -> {
                    if (estado == ZoomSeekBar.ESCALA_PULSADA_DOBLE) {
                        estado = ZoomSeekBar.ESCALA_PULSADA
                    }
                }
                MotionEvent.ACTION_MOVE -> {
                    if (estado == ZoomSeekBar.PALANCA_PULSADA) {
                        valor = ponDentroRango(val_0, escalaMin, escalaMax)
                        invalidate(barRect)
                    }
                    if (estado == ZoomSeekBar.ESCALA_PULSADA_DOBLE) {
                        escalaMin = antVal_0 + (xIni-x_0) * (antVal_0-antVal_1) / (x_0-x_1)
                        escalaMin = ponDentroRango(escalaMin, valMin, valor)
                        escalaMax = antVal_0 + (ancho+xIni-x_0) * (antVal_0-antVal_1) / (x_0-x_1)
                        escalaMax = ponDentroRango(escalaMax, valor, valMax)
                        invalidate()
                    }
                }
            }
        }


        return super.onTouchEvent(event)
    }

    private fun ponDentroRango(val_0: Int, escalaMin: Int, escalaMax: Int): Int {
        if (valor < valMin) {
            return valMin
        } else if (valor > valMax) {
            return valMax
        } else {
            return valor;
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var altoDeseado: Int = altoNumeros + altoRegla + altoBar + getPaddingBottom() + getPaddingTop()
        var alto: Int = obtenDimension(heightMeasureSpec, altoDeseado)
        var anchoDeseado: Int = altoDeseado
        var ancho: Int = obtenDimension(widthMeasureSpec, anchoDeseado)
        setMeasuredDimension(ancho, alto)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    private fun obtenDimension(measureSpec: Int, deseado: Int): Int {
        var dimension: Int = MeasureSpec.getSize(measureSpec)
        var modo: Int = MeasureSpec.getMode(measureSpec);
        if (modo == MeasureSpec.EXACTLY) {
            return dimension;
        } else if (modo == MeasureSpec.AT_MOST) {
            return Math.min(dimension, deseado)
        } else {
            return deseado;
        }
    }
}