package com.example.jimmypalma.audiolibros.data.observables

import android.support.v7.widget.SearchView
import java.util.*

/**
 * Created by jimmypalma on 3/25/19.
 */
class SearchObservable: Observable(), SearchView.OnQueryTextListener {
    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        setChanged()
        notifyObservers(newText)
        return true
    }
}