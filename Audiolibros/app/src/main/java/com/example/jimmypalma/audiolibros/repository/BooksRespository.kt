package com.example.jimmypalma.audiolibros.repository

import com.example.jimmypalma.audiolibros.data.LibroStorage

/**
 * Created by jimmypalma on 4/4/19.
 */
class BooksRespository {
    lateinit var librosStorage: LibroStorage
    constructor(librosStorage: LibroStorage) {
        this.librosStorage = librosStorage
    }

    fun getLastBook(): Int {
        return librosStorage.getLastBook()
    }

    fun hasLastBook(): Boolean {
        return librosStorage.hasLastBook()
    }

    fun saveLastBook(id: Int) {
        return librosStorage.saveLastBook(id)
    }
}