package com.example.jimmypalma.audiolibros.data

import com.example.jimmypalma.audioBooks.entities.Book
import com.example.jimmypalma.audioBooks.entities.Book.Companion.G_TODOS
import com.example.jimmypalma.audiolibros.R

/**
 * Created by jimmypalma on 2/2/19.
 */
object BooksData {
        fun ejemploBooks(): MutableList<Book> {
            val SERVIDOR = "http://mmoviles.upv.es/audiolibros/"
            val Books = ArrayList<Book>()

            Books.add(Book("Kappa", "Akutagawa",
                    R.drawable.kappa, SERVIDOR + "kappa.mp3",
                    Book.G_S_XIX, false, false) as Book)
            Books.add(Book("Avecilla", "Alas Clarín, Leopoldo",
                    R.drawable.avecilla, SERVIDOR + "avecilla.mp3",
                    Book.G_S_XIX, true, false) as Book)
            Books.add(Book("Divina Comedia", "Dante",
                    R.drawable.divinacomedia, SERVIDOR + "divina_comedia.mp3",
                    Book.G_EPICO, true, false) as Book)
            Books.add(Book("Viejo Pancho, El", "Alonso y Trelles, José",
                    R.drawable.viejo_pancho, SERVIDOR + "viejo_pancho.mp3",
                    Book.G_S_XIX, true, true) as Book)
            Books.add(Book("Canción de Rolando", "Anónimo",
                    R.drawable.cancion_rolando, SERVIDOR + "cancion_rolando.mp3",
                    Book.G_EPICO, false, true) as Book)
            Books.add(Book("Matrimonio de sabuesos", "Agata Christie",
                    R.drawable.matrimonio_sabuesos, SERVIDOR + "matrim_sabuesos.mp3",
                    Book.G_SUSPENSE, false, true) as Book)
            Books.add(Book("La iliada", "Homero",
                    R.drawable.iliada, SERVIDOR + "la_iliada.mp3",
                    Book.G_EPICO, true, false) as Book)
            return Books
        }

    var titulo: String = ""
    var autor: String = "anónimo"
    var urlImagen: String = "http://www.dcomg.upv.es/~jtomas/android/audiolibros/sin_portada.jpg"
    var urlAudio: String = ""
    var genero: String = G_TODOS
    var nuevo: Boolean = true
    var leido: Boolean = false

    fun withTitulo(titulo: String): BooksData {
        this.titulo = titulo
        return this
    }

    fun withAutor(autor: String): BooksData {
        this.autor = autor
        return this
    }

    fun withUrlImagen(urlImagen: String): BooksData {
        this.urlImagen = urlImagen
        return this
    }

    fun withGenero(genero: String): BooksData {
        this.genero = titulo
        return this
    }

    fun withUrlAudio(urlAudio: String): BooksData {
        this.urlAudio = urlAudio
        return this
    }

    fun withNuevo(nuevo: Boolean): BooksData {
        this.nuevo = nuevo
        return this
    }

    fun withLeido(leido: Boolean): BooksData {
        this.leido = leido
        return this
    }

    fun build(): Book {
        return Book(titulo, autor, 1, urlAudio, genero, nuevo, leido)
    }
}