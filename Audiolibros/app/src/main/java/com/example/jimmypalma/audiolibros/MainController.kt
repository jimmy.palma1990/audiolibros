package com.example.jimmypalma.audiolibros

import com.example.jimmypalma.audiolibros.data.LibroStorage

/**
 * Created by jimmypalma on 4/1/19.
 */
class MainController(libroStorage: LibroStorage) {
    val libroStorage: LibroStorage = libroStorage

    fun saveLastBook(id: Int) {
        libroStorage.saveLastBook(id)
    }
}